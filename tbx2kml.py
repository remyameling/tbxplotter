import argparse,datetime,os
import include.reader as reader
import include.convert_to_kml as convert_to_kml
import include.convert_to_xls as convert_to_xls
import include.validate as validate
import include.helper as helper
import include.hotspots as hotspots


#   tbx2kml : converteer een .tbx bestand naar een .kml bestand
#
#   - Leest een .tbx bestand in, en :
#       - berekent tijdsverschil, afstand en snelheid tussen 2 opeenvolgende punten
#       - converteert UTC timestamp naar local time
#       - classificeert ieder record als een 'ping' (iedere 2 uur), een 'start' (verschil < stoptime) of een 'default'
#
#   - Groepeert ieder record, per dag
#   - Detectie van ritten (tracks) en stops
#   - Bepalen van top-10 stops




def main(filename, beacon_id, start, end, date, speed, filterstart, filterend, group_per_day, select_all,
         verbose, geocode, hotspots_filename, hotspot_distance):

    if date != "":          # if date was set, overwrite start,end params and set grouped_per_day to false
        start = date
        end = date
        group_per_day = False

    if start != "":         # if start param is set, get date from it
        start_date = start.date()
    else:
        start_date = None

    if end != "":           # if end param is set, get date from it
        end_date = end.date()
    else:
        end_date = None

    if end_date and start_date and end_date < start_date:   # check if end >= start
        print("ERROR: einddatum < startdatum")
        exit(-1)

    if select_all:          # convert boolean select_all to integer
        visible = 1
    else:
        visible = 0

    beacons = reader.read_beacons(filename)     # get all beacons for input file
    if beacon_id != None:                       # if beacon_id filter was set
        beacons = []                            # reset list and
        beacons.append(beacon_id)               # and beacon_id to now empty list


    # read and interprete .tbx data
    print("tbx2kml: Inlezen .tbx bestand : {}".format(filename))


    for beacon_id in beacons:

        records = reader.read_data(filename,beacon_id,start_date,end_date,speed,filterstart,filterend)

        # group records per day
        if group_per_day:
            data = helper.sort_data_per_day(records)
        else:
            data = {}
            data['all dates'] = records

        # get tracks from data
        tracks = helper.get_tracks(data)  # get all tracks

        print("-------------------------------------------------------------------------------------")

        hot_spots = None

        if hotspots_filename is not None:
            hot_spots = hotspots.read_hotspots(hotspots_filename,hotspot_distance)

        success = convert_to_kml.convert(
            data,tracks,beacon_id+".kml",beacon_id,visible, group_per_day, verbose, hot_spots, geocode)

        success = convert_to_xls.convert(data,tracks,beacon_id+".xlsx",beacon_id,group_per_day,geocode)


    

if __name__ == '__main__':

    __author__ = "Remy Ameling"
    __description__ = "Converteer een .tbx bestand naar een excel en eem .kml bestand geschikt voor Google Earth met per dag, ritten en stop plaatsen"
    __date__ = "20200419"

    parser = argparse.ArgumentParser(description=__description__ , epilog="Ontwikkeld door {} op {}".format(__author__ , __date__ ))

    parser.add_argument('filename', type=str, help='Naam van de .tbx file.')

    parser.add_argument('-g','--geocode', action='store_true',help="Reverse geocode GPS data, default = False")
    parser.add_argument('-d','--per_day', action='store_true', help="Groepeer data per dag")
    parser.add_argument('-s','--selectall', action='store_true', help="Select alle data in google earth")
    parser.add_argument('-v', '--verbose', action='store_true', help="Verbose output")

    parser.add_argument('--hotspots', type=str, help='Naam van een hotspots file.')
    parser.add_argument('--hotspots_distance', help='Afstand in meters voor hotspots', default=200)
    parser.add_argument('--beaconid', type=str, help='Naam van het baken.', default=None)

    parser.add_argument('--date', type=validate.date, help='Datum voor selectie (dd-mm-yyyy)', default="")
    parser.add_argument('--start',type=validate.date, help='Start datum (dd-mm-yyyy)', default="")
    parser.add_argument('--end', type=validate.date, help='Eind datum (dd-mm-yyyy)', default="")
    parser.add_argument('--speed',type=int, help='Minimale gemiddelde snelheid voor beweging in km/u (default = 5)',
                        default=5)

    parser.add_argument('--filterstarttime', type=validate.time, help='Filter voor start tijdsblok (hh:mm)', default="")
    parser.add_argument('--filterendtime', type=validate.time, help='Filter voor einde tijdsblok (hh:mm)', default="")

    args = parser.parse_args()

    main(args.filename, args.beaconid, args.start, args.end, args.date, args.speed,
         args.filterstarttime, args.filterendtime, args.per_day,
         args.selectall, args.verbose, args.geocode, args.hotspots, args.hotspots_distance)