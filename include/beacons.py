import sqlite3,datetime


beaconsDbHandle = None

def beacons_db_create_tables(dbcon):
    
    cursor = dbcon.cursor()

    cursor.execute('''
        CREATE TABLE beacons(beaconid TEXT PRIMARY KEY,friendlyname TEXT,last_splitted DATETIME)
    ''')

    dbcon.commit()


def beaconsdb_get_handle():
    global beaconsDbHandle

    if not beaconsDbHandle:

        beaconsDbHandle = sqlite3.connect('beacons.sqlite')
        
        try:
            beacons_db_create_tables(beaconsDbHandle)
            
        except sqlite3.OperationalError:
            
            return beaconsDbHandle

        return beaconsDbHandle

    else:
        return beaconsDbHandle


def get_beacon(beacon_id):

    dbcon = beaconsdb_get_handle()
    c = dbcon.cursor()

    c.execute('''SELECT beaconid,friendlyname,DATETIME(last_splitted,'unixepoch') as last_splitted FROM beacons WHERE beaconid = ?''', (beacon_id,))

    res = c.fetchone()

    if res:
        return res
    else:
        return None

def insert_or_replace_beacon(beaconid, friendlyname, last_splitted):


    if last_splitted != None:
        epoch = datetime.datetime.utcfromtimestamp(0)
        timestamp = datetime.datetime.strptime(last_splitted, "%d-%m-%y %H:%M:%S")

        last_splitted_epoch = (timestamp - epoch).total_seconds()
    else:
        last_splitted_epoch = None


    dbcon = beaconsdb_get_handle()
    cursor = dbcon.cursor()

    res = cursor.execute('''
        INSERT OR REPLACE INTO beacons(beaconid,friendlyname,last_splitted) values (?,?,?)
        ''', (beaconid, friendlyname, last_splitted_epoch))

    dbcon.commit()

