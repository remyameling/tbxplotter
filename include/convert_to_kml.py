import logging, operator, csv,os
import datetime as dt
import include.geo as geo
import include.geocode as geocode
from simplekml import Kml, Snippet, Types, Color, AltitudeMode
from polycircles import polycircles

TIME_ISO = "%Y-%m-%dT%H:%M:%S"
TIME_HMS = "%H:%M:%S"
TIME_HM  = "%H:%M"
TIME_DMY = "%d-%m-%Y"

#
# Converteer
#



def get_time_diff_from_isotimestring(isotimestring1, isotimestring2):

    # returns the time difference (in minutes) from two time strings in ISO format (TIME_ISO)

    t1 = dt.datetime.strptime(isotimestring1, TIME_ISO)
    t2 = dt.datetime.strptime(isotimestring2, TIME_ISO)
    delta = t2-t1

    return round(delta.seconds/60,2)

def get_track_distance(coords):

    # computes the distance in meters of a track.
    # the distance is the sum of all distances between the individual points

    prev = None
    distance = 0

    for coord in coords:
        if prev:
            distance = distance + geo.get_distance((coord[0],coord[1]),(prev[0],prev[1]))
        prev = coord

    return distance

def add_track(tracknumber, doc, when, coords, visible):

    # adds a single track to a KML folder
    #
    # in:   tracknumber,    number of the track
    #       doc,            the KML document
    #       when,           list of timestamps
    #       coords,         list of coordinates
    #       visible,        visibility of the folder and all childs

    date = str(dt.datetime.strptime(when[0], TIME_ISO).strftime("%d-%m"))   # date of the track
    start = str(dt.datetime.strptime(when[0], TIME_ISO).strftime("%H:%M"))  # start time
    end = str(dt.datetime.strptime(when[-1], TIME_ISO).strftime("%H:%M"))   # end time

    duration = get_time_diff_from_isotimestring(when[0],when[-1])           # duration of the track
    distance = round(get_track_distance(coords)/1000,1)                     # distance in KM of the track
    numpoints = len(when)                                                   # number of points in the track

    trackname = "rit # {:02d} ({} km)".format(tracknumber,distance)
    trackinfo = "op {} van {} tot {} \n{} min., {} km, {} coord.".format(date,start,end,duration,distance,numpoints)

    trk = doc.newgxtrack(name=trackname,description=trackinfo)              # Create a new track in the folder

    # Add all the information to the track

    trk.newwhen(when)  # Each item in the give nlist will become a new <when> tag
    trk.newgxcoord(coords)  # Ditto

    # Styling

    trk.stylemap.normalstyle.iconstyle.icon.href = 'http://earth.google.com/images/kml-icons/track-directional/track-0.png'
    trk.stylemap.normalstyle.linestyle.color = 'ffffac59'
    trk.stylemap.normalstyle.linestyle.width = 8
    trk.stylemap.highlightstyle.iconstyle.icon.href = 'http://earth.google.com/images/kml-icons/track-directional/track-0.png'
    trk.stylemap.highlightstyle.iconstyle.scale = 1.2
    trk.stylemap.highlightstyle.linestyle.color = 'ffffac59'
    trk.stylemap.highlightstyle.linestyle.width = 8
    trk.visibility = visible



def add_tracks(tracks,doc,visible,group_per_day):

    # add all tracks to the kml file
    #
    # in:   tracks, dictionary (per day) of records
    #       doc, the KML doc where the tracks should be added
    #       visible, visibility of the folder and child elements
    #       group_per_day, group tracks per day (True) or not (False)


    doc = doc.newdocument(name='Ritten', visibility=visible)            # add 'Tracks' folder
    num_tracks = 0

    for day,tracks_per_day in tracks.items():

        if group_per_day:
            folder = doc.newfolder(name=day,visibility=visible)         # Create a folder per day
        else:
            folder = doc

        for tracknumber,track in tracks_per_day.items():

            when, coords = track[0],track[1]
            add_track(tracknumber, folder, when, coords, visible)       # add the track
            num_tracks += 1


    return num_tracks


def round_coord(lat,long):

    lat = round(lat,2)
    long = round(long,2)

    return (lat,long)

def format_duration_in_seconds(duration_in_seconds):

    now = dt.datetime.now()
    x = dt.datetime.now() + dt.timedelta(seconds=duration_in_seconds)

    deltastr = str(x-now)

    return deltastr.split('.')[0]

def match_point_to_hotspots(pos,hotspots):

    names = []
    for hotspot in hotspots:

        if geo.point_in_circle(pos,{'lat':hotspot['lat'],'long':hotspot['long']},int(hotspot['dist'])):
            # print("{} in hotspot {}".format(pos,hotspot['name']))
            names.append(hotspot['name'])

    return names

def add_stops(data, kml, group_per_day, visible, verbose, hotspots, lookup = False):

    # determina all stops and add them to a list (stops) and to the KML document as points
    #
    # in:   data, dictionary (per day) of records
    #       kml, the parent KML document
    #       group_per_day, group records per day (True) or not (False)
    #       visible, visibility of the document and all child elements
    #       verbose, verbose output
    #       hotspots, dict. of hotspots
    #       lookup, use geocoding or not

    numStops = 0
    stops = []
    doc = kml.newdocument(name='Stops', )  # add 'Stops' folder
    matched_hotspots = None

    for day, records in data.items():

        if group_per_day:
            folder = doc.newfolder(name=day,visibility=0)   # Create a folder per day
        else:
            folder = doc

        prev_record = []
        duration,start_time = 0,0

        if verbose:
            print("-------------------")
            print("Stops op {}".format(day))
            print("-------------------")

        for record in records:

            if prev_record:
                if record['type'] == 'stop':
                    if prev_record['type'] == 'move' or prev_record['type'] == '':  # if previous was a move or undefined (the first record)
                        # begin of a new stop

                        duration = 0                                                    # reset duration
                        start_time = record['timestamp']                                # save start time,
                        start_lat = record['pos'][0]                                    # lat,
                        start_long = record['pos'][1]                                   # and long

                elif start_time != 0:
                    # end of a stop, compute duration and name of the stop; then save the stop

                    now = record['timestamp']
                    duration = (now - start_time).total_seconds()
                    pnt = folder.newpoint(visibility=visible,coords=[(start_lat,start_long)])    # create a new point


                    if hotspots is not None:
                        matched_hotspots = match_point_to_hotspots((start_lat,start_long),
                                                                   hotspots)            # match this stop to
                                                                                        # list of hotspots

                    pnt.name = "{} op {}".format(
                        str(format_duration_in_seconds(duration)),
                        dt.datetime.strftime(record['timestamp'], TIME_DMY)
                    )

                    address = geocode.get_address((start_lat,start_long), lookup)

                    pnt.description = "{} (van {} tot {}) : {} {}, {}".format(
                        str(format_duration_in_seconds(duration)),
                        dt.datetime.strftime(start_time, TIME_HM),
                        dt.datetime.strftime(record['timestamp'], TIME_HM),
                        address['street_name'],
                        address['street_number'],
                        address['locality']
                    )

                    if verbose:
                        print("{} (van {} tot {}) : {} {}, {}".format(
                            str(format_duration_in_seconds(duration)),
                            dt.datetime.strftime(start_time, TIME_HM),
                            dt.datetime.strftime(record['timestamp'], TIME_HM),
                            address['street_name'],
                            address['street_number'],
                            address['locality'])
                        )

                    pos_name = "{} ({})".format(dt.datetime.strftime(record['timestamp'], TIME_DMY),
                                                str(format_duration_in_seconds(duration)))

                    pos = {'lat': start_lat, 'long': start_long, 'name': pos_name,
                           'duration': str(format_duration_in_seconds(duration)),
                           'datetime':"op {} van {} tot {}".format(dt.datetime.strftime(record['timestamp'], TIME_DMY),
                                                             dt.datetime.strftime(start_time, TIME_HMS),
                                                             dt.datetime.strftime(record['timestamp'], TIME_HMS)),
                           'hotspots': matched_hotspots}                                # create a new stop pos
                    stops.append(pos)                                                   # append to the list
                    numStops += 1                                                       # increase number of stops
                    start_time = 0                                                      # reset start time

            prev_record = record

    return stops


def find_grid_origin(points):

    # given a list of coordinates, determine the origin (= most bottom left position)

    first = points[0]
    min_x = first['lat']
    min_y = first['long']

    for point in points:
        if point['lat'] < min_x:
            min_x = point['lat']
        if point['long'] < min_y:
            min_y = point['long']

    return {'x' : min_x,'y' : min_y}

def create_grid(points, origin, size):

    # create the grid as a 2-dimensinal dictionary with key = x,y and value is 'number of stops'
    # foreach point in points, compute to which 'grid cell' it belongs
    # and incremet the grid value

    grid = {}
    for point in points:

        # determine x-coordinate of cluster
        horizontal_distance = point['lat'] - origin['x']
        x = int(horizontal_distance / size)

        # determine y-coordinate of cluster
        vertical_distance = point['long'] - origin['y']
        y = int(vertical_distance / size)

        if x in grid:
            if y in grid[x]:
                grid[x][y] += 1
            else:
                grid[x][y] = 1
        else:
            grid[x] = {}
            grid[x][y] = 1

    return grid

def add_top_N_clusters(clusters, grid_size, n, doc, visible, color = Color.yellow):

    # render the first N clusters as a rectangle (square) of given size
    #
    # in:   clusters, sorted dictionary containing the clusters (bottom left, top right and number of elements
    #       grid_size, size of cluster in degrees
    #       n, number of clusters to render
    #       doc, KML root document
    #       visible, document and child visibility

    areas = doc.newfolder(name="areas", visibility=visible)
    labels = doc.newfolder(name="labels", visibility=visible)

    num_values = 0
    for key,item in clusters:

        if num_values < n:
            x1, y1, x2, y2, num_stops = item[0], item[1], item[2], item[3], item[4]

            # create the cluster as a rectangle (polygon)
            pol = areas.newpolygon(name=str(num_stops) + " stops", visibility=visible)
            pol.outerboundaryis = [(x1, y1), (x1, y2), (x2, y2), (x2, y1), (x1, y1)]
            pol.style.polystyle.color = Color.changealphaint(100, color)

            # create a dummy point in the middle of the cluster as a label

            label = labels.newpoint(name=str(num_stops) + " stops", visibility=visible)
            label.coords = [(x1 + grid_size / 2, y1 + grid_size / 2)]

        num_values += 1


def cluster_stops(stops, kml, visible):

    GRID_SIZE = 0.0019        # grid length and width
    TOP_N = 10

    # group stops within clusters of DEGREE degrees; create a KML folder of the top TOP-N stops
    #
    # in:   stops, list of all stops
    #       kml, the KML folder where the top-N stops should be added
    #       visible, folder and child element visibility

    # create a new folder
    doc = kml.newdocument(name='Top {} stops'.format(TOP_N), visibility=visible)  # add 'Top-N' Stops folder

    # find the grid's origin (bottom left)
    origin = find_grid_origin(stops)

    # create the grid
    grid = create_grid(stops, origin, GRID_SIZE)

    # now create a new dictionary with a floating point number as a key, so we can sort by number of stops
    # integer part of floating point number = num stops
    # decimal part of floating point number = concatanation of x and y (to make the key unique)
    # value of the dictionary is a tupple of bottom left and top right coordinate, number of stops
    vals = {}
    for x, yvals in grid.items():
        for y in yvals:

            num_stops = grid[x][y]

            x1 = x * GRID_SIZE + origin['x']
            x2 = (x+1) * GRID_SIZE + origin['x']
            y1 = y * GRID_SIZE + origin['y']
            y2 = (y+1) * GRID_SIZE + origin['y']

            key = str(num_stops)+"."+str(x)+str(y)  # note: concatenation of x and y could be non-unique but ignore this for now.
            vals[float(key)] = (x1,y1,x2,y2,num_stops)


    # sort vals by key
    sorted_by_value = sorted(vals.items(), key=lambda kv: kv[0], reverse=True)

    # finaly, render the clusters
    add_top_N_clusters(sorted_by_value, GRID_SIZE, TOP_N, doc, visible)

def add_hotspots(hotspots, stops, kml, visible, verbose = False):

    # create a new folder
    doc = kml.newdocument(name='Hotspots', visibility=visible)  # add 'Hotspots' folder
    foundHotspot = False

    print("----------------------------- Hotspots: ---------------------------------------------")

    for row in hotspots:
        name, lat, long, distance = row['name'], row['lat'], row['long'], row['dist']

        folder = doc.newdocument(name=name, visibility=visible)  # add 'Hotspots' folder
        polycircle = polycircles.Polycircle(latitude=float(lat),longitude=float(long),radius=int(distance),number_of_vertices=36);
        spot = folder.newpolygon(name=name,outerboundaryis=polycircle.to_kml(),visibility=visible)
        spot.style.polystyle.color = Color.changealphaint(200, Color.green)

        for stop in stops:
            if name in stop['hotspots']:

                point = (stop['lat'],stop['long'])
                folder.newpoint(visibility=visible,coords=[point],name=stop['name'])    # create a new point

                if verbose:
                    print("Stop in de nabijheid van {} op {} ({})".format(name,stop['datetime'],stop['duration']))
                    foundHotspot = True


    if foundHotspot:
        print("-------------------------------------------------------------------------------------")



def convert(data, tracks, output_filename, root_folder_name, visible, group_per_day = True, verbose = False, hot_spots = None, lookup = False):

    # converts data into a .kml file, with separate folders for 'tracks', 'stops' and 'top-10 stops'

    # in:   data, list of records as output from .tbx reader
    #       output_filename, name of the .kml file to generate
    #       root_folder_name, name of root folder
    #       visible, if 1, marks all artifacts as selected
    #       group_per_date, groups all records per day
    #       verbose, verbose output
    #       hotspots, hotspots file
    #       lookup, use geocoding or not
    
    try:

        num_days = len(data)


        kml = Kml(name=root_folder_name, open=1)                                        # create the KML root doc
        num_tracks = add_tracks(tracks, kml, visible, group_per_day)                    # add tracks to the kml doc
        stops = add_stops(data, kml, group_per_day, visible, verbose, hot_spots, lookup) # add stops
        num_stops = len(stops)

        if num_stops > 0:
            cluster_stops(stops, kml, visible)                                      # add TOP-10 stops

        if hot_spots is not None:
            add_hotspots(hot_spots, stops, kml, visible, verbose)                   # add hot-spots

        print("van {} tot en met {} : {} dagen ingelezen met {} ritten en {} stops.".format(
            list(data.keys())[0],
            list(data.keys())[-1],
            num_days,
            num_tracks,
            num_stops)
        )
        print("-------------------------------------------------------------------------------------")
        print("KML data opgeslagen in {}".format(output_filename))

        # Save the kml to file
        kml.save(output_filename)                                           # save the KML file

        return True

    except Exception as exc:

        logging.exception("Error converting data")
        return False
