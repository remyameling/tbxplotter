def sort_data_per_day(data):

    # create a dictionary of records per day, assume records in data are asscending

    days = {}
    records = []
    current = data[0]['timestamp'].strftime("%d-%m-%Y")     # current date

    for record in data:

        date = record['timestamp'].strftime("%d-%m-%Y")     # get date from record

        if current == date:           # if same date
            records.append(record)    # append

        else:                         # else start a new date

            days[current] = records
            current  = date
            records = [record]

    days[current] = records

    return days

def get_tracks(data):

    # returns a dictionary (per day) of dictionaries of tracks, where a track is a list of timestamp,positions tuples
    #
    # in:   data, dictionary (per day) of records

    tracks = {}
    for day,records in data.items():

        tracks_per_day,when,coords,track_number,prev_record = {},[],[],1,None

        for record in records:
            if prev_record:                                         # if this is not the first record
                if record['type'] == 'move':                            # if this is a move
                    if prev_record['type'] == 'stop'  and when and coords:
                                                                            # if this is the start of a new track
                                                                            # AND there is data

                        tracks_per_day[track_number] = (when,coords)        # store previous track
                        when,coords = [],[]                                 # reset data
                        track_number += 1                                   # increase track number

                    when.append(record['timestamp'].strftime("%Y-%m-%dT%H:%M:%S"))
                                                                        # append timestamp
                    coords.append(record['pos'])                        # append coordinates

            prev_record = record

        if when and coords:                                              # if there is data left
            tracks_per_day[track_number] = (when,coords)

        tracks[day] = tracks_per_day

    return tracks