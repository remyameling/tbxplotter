import logging, operator
import datetime as dt
import include.geocode as geocode
import xlsxwriter
import time


TIME_ISO = "%Y-%m-%dT%H:%M:%S"
TIME_HMS = "%H:%M:%S"

def add_header(worksheet,doc):

    default_format = doc.add_format({'bold': True, 'font_color': 'black'})


    worksheet.write('A1', "datum",default_format)
    worksheet.write('B1', "tijd",default_format)

    worksheet.write('C1', "voertuig",default_format)
    worksheet.write('D1', "m/s",default_format)
    worksheet.write('E1', "sat",default_format)

    worksheet.write('F1', "rit nr",default_format)


    worksheet.write('G1', "adres", default_format)
    worksheet.write('H1', "land",default_format)
    worksheet.write('I1', "straat",default_format)
    worksheet.write('J1', "huisnummer",default_format)
    worksheet.write('K1', "plaats",default_format)
    worksheet.write('L1', "gemeente",default_format)
    worksheet.write('M1', "snelheid",default_format)
    worksheet.write('N1', "afstand",default_format)
    worksheet.write('O1', "tijdsvershil (h:m:s)",default_format)


    worksheet.write('Q1', "type", default_format)
    worksheet.write('R1', "lat", default_format)
    worksheet.write('S1', "long", default_format)


def add_data(data,doc,group_per_day,internet_lookup=True):

    start_format = doc.add_format({'bold': True, 'bg_color': 'yellow'})
    start_format_date = doc.add_format({'bold': True, 'bg_color': 'yellow','num_format': 'dd/mm/yyyy'})
    start_format_time = doc.add_format({'bold': True, 'bg_color': 'yellow','num_format': 'HH:mm:ss'})

    default_format = doc.add_format({'bold': False, 'font_color': 'black'})
    default_format_date = doc.add_format({'bold': False, 'font_color': 'black','num_format': 'dd/mm/yyyy'})
    default_format_time = doc.add_format({'bold': False, 'font_color': 'black','num_format': 'HH:mm:ss'})

    ping_format = doc.add_format({'bold': False, 'font_color': '#dddddd'})
    ping_format_date = doc.add_format({'bold': False, 'font_color': '#dddddd', 'num_format': 'dd/mm/yyyy'})
    ping_format_time = doc.add_format({'bold': False, 'font_color': '#dddddd', 'num_format': 'HH:mm:ss'})


    if not group_per_day:
        default_worksheet = doc.add_worksheet("all")  # create the worksheet

    for day,tracks_per_day in data.items():

        if group_per_day:
            ws = doc.add_worksheet(day)              # create the worksheet per day
        else:
            ws = default_worksheet

        add_header(ws,doc)

        rownum,track_counter,prev_item = 2,0,None

        for item in tracks_per_day:

            if prev_item and (prev_item['type'] == 'stop' or prev_item['type'] == '') and item['type'] == 'move':  # if start of a new track

                    cell_format = start_format
                    date_format = start_format_date
                    time_format = start_format_time
                    track_counter += 1

            elif item['type'] == '':

                cell_format = ping_format
                date_format = ping_format_date
                time_format = ping_format_time
                #ws.set_row(rownum-1, None, None, {'hidden': 1})

            else:

                cell_format = default_format
                date_format = default_format_date
                time_format = default_format_time

            address = geocode.get_address(item['pos'], internet_lookup)

            stampdate = item['timestamp'].strftime(TIME_ISO)
            stampdate = dt.datetime.strptime(stampdate, TIME_ISO)
            times  = dt.datetime.strftime(stampdate,"1970-01-01T%H:%M:%S")
            stamptime = dt.datetime.strptime(times,TIME_ISO)


            ws.write('A' + str(rownum), stampdate, date_format)
            ws.write('B' + str(rownum), stamptime, time_format)
            ws.write_string('C' + str(rownum), "-", cell_format)                # TODO: add voertuig
            ws.write_string('D' + str(rownum), item['sm'], cell_format)
            ws.write('E' + str(rownum), item['num-fix'], cell_format)
            ws.write('F' + str(rownum), track_counter, cell_format)             # TODO: add rit_nr

            ws.write_string('Q' + str(rownum), item['type'], cell_format)
            ws.write('R' + str(rownum), item['pos'][0], cell_format)
            ws.write('S' + str(rownum), item['pos'][1], cell_format)


            if address:

                url = 'http://maps.google.com/maps?q='+str(item['pos'][1])+","+str(item['pos'][0])

                ws.write_url('G' + str(rownum), url, cell_format, string=address['formatted_address'])
                ws.write_string('H' + str(rownum), address['country'], cell_format)
                ws.write_string('I' + str(rownum), address['street_name'], cell_format)
                ws.write_string('J' + str(rownum), address['street_number'], cell_format)
                ws.write_string('K' + str(rownum), address['locality'], cell_format)
                ws.write_string('L' + str(rownum), address['city'], cell_format)

            ws.write('M' + str(rownum), item['speed'],cell_format)
            ws.write('N' + str(rownum), item['distance'],cell_format)

            diff = time.strftime('%H:%M:%S', time.gmtime(item['diff']))

            ws.write_string('O' + str(rownum), diff,cell_format)


            rownum += 1
            prev_item = item

        num_api_calls = geocode.get_api_calls()
        if num_api_calls > 0:
            print("{} calls (totaal) naar geocode API".format(num_api_calls))

        ws.set_column('Q:S',None,None,{'hidden': 1})
        ws.set_column('A:A', 12)
        ws.set_column('D:F', 5)
        ws.set_column('G:G', 50)
        ws.set_column('H:I', 20)
        ws.set_column('L:L', 20)


def convert(data, tracks, output_filename, root_folder_name, group_per_day = True,internet_lookup = True):

    # converts data into a .xls file, with separate tabs per day

    # in:   data, list of records as output from .tbx reader
    #       output_filename, name of the .xls file to generate
    #       root_folder_name, name of root folder
    #       group_per_date, groups all records per day
    
    try:

        num_days = len(data)

        workbook = xlsxwriter.Workbook(output_filename)

        add_data(data, workbook, group_per_day,internet_lookup)     # add tracks to the worksheet

        # Save the workbook
        workbook.close()                                            # save the XLS file

        print("Excel data opgeslagen in {}".format(output_filename))

        return True

    except Exception as exc:

        logging.exception("Error converting data")
        return False
