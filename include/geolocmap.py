from math import sin, cos, sqrt, atan2, radians, asin
from include import geo
#import include.geo as geo

#
# maps a geographical location to a unique key, using a specified cluster/key size in metres
#

KEY_SIZE = 50
KEY_PREFIX = "idx_"

def get_radians_key_size(key_size):

    return geo.get_radions_by_distance(key_size)

def get_latitude_key_length(key_size):

    return get_radians_key_size(key_size)


def get_longitude_key_length(latitude,key_size):

    operand = 2 * sin (get_radians_key_size(key_size) / 2) / cos( geo.get_radions_by_degrees(latitude))

    if operand > geo.M_PI/2:
        return 1
    else:
        return asin(operand)

def map_location_to_key(location,key_size=KEY_SIZE):

    # maps a location to a unique key, using specified KEY_SIZE in metres
    #
    # in:   location, tuple containing (longtitude,latitude)
    #       key_size, size for the projection (in meters)


    latitude_radions  = geo.get_radions_by_degrees(location[1])
    longtitude_radions = geo.get_radions_by_degrees(location[0])

    keyLatitude = round(latitude_radions / get_latitude_key_length(key_size))
    keyLongitude = round(longtitude_radions / get_longitude_key_length(location[1],key_size))

    return KEY_PREFIX+"key-slot-size-"+str(key_size)+"-lat-"+str(keyLatitude)+"-lng-"+str(keyLongitude)