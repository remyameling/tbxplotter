import datetime,argparse

def date(s):
    try:
        if s == "":
            return ""
        else:
            return datetime.datetime.strptime(s, "%d-%m-%Y")

    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)

def time(s):
    try:
        if s == "":
            return ""
        else:
            return datetime.datetime.strptime(s, "%H:%M")

    except ValueError:
        msg = "Not a valid time: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


def dt(s):
    try:
        if s == "":
            return ""
        elif len(s) == 8:
            s = s+" 00:00:00"

        return datetime.datetime.strptime(s, "%d-%m-%y %H:%M:%S")

    except ValueError:
        msg = "Not a valid time: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


def bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')