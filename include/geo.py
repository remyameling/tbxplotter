from math import sin, cos, sqrt, atan2, radians, asin
import include.geocode as geocode

EARTH_RADIUS = 6371000
M_PI = 3.14159265358979323846

def get_distance(position1,position2):

    # returns the distance between two geographical positions
    # position1, postion2 are tuples (longtitude,lattitude)
    #

    R = EARTH_RADIUS/1000

    lat1 = radians(position1[1])
    lon1 = radians(position1[0])

    lat2 = radians(position2[1])
    lon2 = radians(position2[0])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c * 1000

    return distance

def get_radions_by_distance(distance):

    return distance / EARTH_RADIUS

def get_radions_by_degrees(degree):

    return (M_PI / 180) * degree

def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    r = EARTH_RADIUS # Radius of earth in kilometers. Use 3956 for miles
    return c * r

def point_in_circle(point,center,radius):

    lat1 = float(center['lat'])
    lon1 = float(center['long'])

    lat2 = float(point[1])
    lon2 = float(point[0])

    a = haversine(lon1, lat1, lon2, lat2)

    if a <= radius:
        # print("{} {} in hotspot with {},{} and distance {} (a = {})".format(lat2,lon2,lat1,lon1,radius,a))
        return True
    else:
        # print("{} {} not in hotspot with {},{} and distance {} (a = {})".format(lat2,lon2,lat1,lon1,radius,a))
        return False



