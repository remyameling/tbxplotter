import xml.etree.ElementTree as elmenttree
import logging,os,csv

def read_hotspots_kml_placemark(Placemark,default_distance):

    data = {}

    for placemarkchilds in Placemark:
        if placemarkchilds.tag == '{http://www.opengis.net/kml/2.2}name':
            data['name'] = placemarkchilds.text
            data['dist'] = default_distance
        elif placemarkchilds.tag == '{http://www.opengis.net/kml/2.2}LookAt':
            for lookatchilds in placemarkchilds:
                if lookatchilds.tag == '{http://www.opengis.net/kml/2.2}longitude':
                    data["long"] = lookatchilds.text
                if lookatchilds.tag == '{http://www.opengis.net/kml/2.2}latitude':
                    data["lat"] = lookatchilds.text


    return data


def read_hotspots_kml_folder(Folder,default_distance):
    data = []

    for child in Folder:
        if child.tag == '{http://www.opengis.net/kml/2.2}Placemark':
            data.append(read_hotspots_kml_placemark(child,default_distance))

    return data


def read_hotspots_kml(file_name,default_distance):
    data = []

    try:

        tree = elmenttree.parse(file_name)
        kml = tree.getroot()
        for document in kml:
            for child in document:
                if child.tag == '{http://www.opengis.net/kml/2.2}Folder':
                    data = read_hotspots_kml_folder(child,default_distance)



    except Exception as exc:
        logging.exception("Error reading from file: {}".format(file_name))
        exit(1)

    return data

def read_hotspots_txt(file_name):
    data = []

    try:
        csvfile = open(file_name)
        reader = csv.reader(csvfile, delimiter=',')
        try:
            for row in reader:
                data.append({ 'name' : row[0], 'lat': row[1], 'long' : row[2], 'dist' : row[3]})
        except Exception as exc:
            logging.exception("Fout in formaat van hotspots file {}".format(file_name))
            exit(2)

    except Exception as exc:
        logging.exception("Error reading from file: {}".format(file_name))
        exit(1)

    return data


def read_hotspots(file_name,default_distance):

    filename, file_extension = os.path.splitext(file_name)

    if file_extension == '.kml':
        return read_hotspots_kml(file_name,default_distance)
    else:
        return read_hotspots_txt(file_name)
