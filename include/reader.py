import csv,logging,datetime
import include.geo as geo
from dateutil import tz


def convert_single_coord(value,orient):

    # gets a lat or long coordinate and converts it to decimal degrees

    parts = value.split('.')

    degrees = float(parts[0][:-2])
    minutes = float(parts[0][-2:] + '.' + parts[1])
    coord = round(degrees + minutes / 60, 6)

    if (orient == 'S') or (orient == 'W'):
        coord = latitude * -1

    return coord


def convert_utc_date_time_to_local_timestamp(date,time,format="%Y-%m-%d %H:%M:%S",timeZone="Europe/Amsterdam"):

    # gets the timestamp from date and time, and converts this to local time
    # in the format
    # assuming timezone = 'Europe/Amsterdam'

    utc_zone = tz.gettz('UTC')
    local_zone = tz.gettz(timeZone)
    
    dat = "20"+date[4:6]+"-"+date[2:4]+"-"+date[0:2]
    tim = time[0:2]+":"+time[2:4]+":"+time[4:6]

    utc = datetime.datetime.strptime(dat+' '+tim,format)
    utc = utc.replace(tzinfo=utc_zone)
    local = utc.astimezone(local_zone)

    return local


def get_timediff(starttime,endtime):

    # returns the difference in time of two timestamps, in seconds

    return (endtime - starttime).seconds


def get_type(curTimeStamp, prevTimeStamp, prevType, stoptime, ping_time=6300):

    # get the record type ('ping', 'start' or 'default')

    diff = get_timediff(prevTimeStamp,curTimeStamp)

    if diff >= ping_time:
        return "ping"

    if prevType == "ping" or diff > stoptime:
        return "start"

    return "default"

def get_type_new(distance,timediff,speed,stop_speed):

    # get the record type ('stop' or 'move')

    if speed < stop_speed:
        return "stop"
    else:
        return "move"

def filter_by_beacon(filename,target_beacon_id):

    data = []

    try:
        csvfile = open(filename)

        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:

            beacon = row[1]
            beacon_id = beacon.split(":")[1]

            if target_beacon_id == beacon_id:

                data.append(row)

        return data

    except Exception as exc:

        logging.exception("Error reading from file: {}".format(filename))
        exit(1)


def read_beacons(filename):

   data = {}
   beacons = []

   try:
        csvfile = open(filename)

        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:

            beacon = row[1]
            beaconid = beacon.split(":")[1]
            data[beaconid] = beaconid

        for key in data:
            beacons.append(key)

        return beacons

   except Exception as exc:

       logging.exception("Error reading from file: {}".format(filename))
       exit(1)



def read_data(file_name, beacon_id, start_date, end_date, stop_speed, starttime, endtime, speed_max=200):

    # read the .tbx file per line and filter on start_date and end_date, if specified.
    # compute distance, timedifference, speed and classify each coordinate as 'ping', 'stop' or 'default'
    # store records in new list (data)
    #
    # in : file_name (input file name)
    #      beacon_id (name of the beacon to read)
    #      start_date (process only records >= start_date, optional)
    #      end_date (process only records <= end_date, optional)
    #      stop_speed (max avg speed before considered as a stop)
    #      speed_max (max speed between 2 succeeeding postions.
    #                 If max time is exceeded, the position is discarded, optional)
    
    try:
        csvfile = open(file_name)
        reader = csv.reader(csvfile,delimiter=',')
        
        data = []               # list
        prev_record = {}        # previous record to compute distance, duration, speed and type

        for row in reader:

            beacon,date,time,lat,lat_orientation,long,long_orientation,stop_move,num_fix = \
                row[1],row[3],row[4],row[5],row[6],row[7],row[8],row[9],row[10]

            beaconID = beacon.split(":")[1]

            if (beaconID == beacon_id):

                timestamp = convert_utc_date_time_to_local_timestamp(date,time,"%Y-%m-%d %H:%M:%S")

                # if a start or end date was specified,
                # filter only records after start_date or before or equal end_date

                bAddRecord = True

                if (start_date != None) and (timestamp.date() < start_date):
                    bAddRecord = False

                if (end_date != None) and (timestamp.date() > end_date):
                    bAddRecord = False

                # if a filter on time was set
                if starttime:
                    filterstarttime = datetime.datetime.strptime(date+"T"+starttime.strftime("%H:%M:%S"),"%d%m%yT%H:%M:%S")
                    if timestamp.time() <= filterstarttime.time():
                        bAddRecord = False

                # if a filter on time was set
                if endtime:
                    filterendtime = datetime.datetime.strptime(date + "T" + endtime.strftime("%H:%M:%S"), "%d%m%yT%H:%M:%S")
                    if timestamp.time() > filterendtime.time():
                        bAddRecord = False


                if bAddRecord:  # if record is within the filter

                    # compute latitude, longtitude

                    lat = convert_single_coord(lat,lat_orientation)         # latitude
                    long = convert_single_coord(long,long_orientation)      # longtitude
                    pos = (long,lat)

                    if prev_record:     # if there was a previous record, compute distance, speed etc

                        distance = int(geo.get_distance(pos,prev_record['pos']))  # distance in meter
                        timediff = get_timediff(prev_record['timestamp'], timestamp) # time in seconds
                        speed = int((distance/1000)/(timediff/3600)) # speed in km/h
                        # recordtype = get_type(timestamp,prev_record['timestamp'], prev_record['type'], stop_time)
                        recordtype = get_type_new(distance, timediff, speed, stop_speed)

                    else:               # otherwise reset to 0
                        distance = 0
                        timediff = 0
                        speed = 0
                        recordtype = ''

                    if speed_max == None or speed <= speed_max:    # if speed > speed_max, ignore the record as it is most probably a GPS error

                        record = {  "pos" : pos,
                                    "distance" : distance,
                                    "diff" : timediff,
                                    "speed":speed,
                                    "timestamp" : timestamp,
                                    "sm": stop_move,
                                    "num-fix" : num_fix,
                                    "type" : recordtype,
                                 }

                    prev_record = record
                    data.append(record)

        return data

    except Exception as exc:

        logging.exception("Error reading from file: {}".format(file_name))
        exit(1)

