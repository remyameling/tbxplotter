import requests,json
import sqlite3
import include.geolocmap as geolocmap

geocodeDbHandle = None
api_call_counter = 0

def get_api_calls():
    global api_call_counter

    return api_call_counter

def lookup_address_via_google_api(location):

    global api_call_counter

    sensor = 'true'
    base = "https://maps.googleapis.com/maps/api/geocode/json?"
    params = "latlng={lat},{lon}&sensor={sen}&key={key}".format(
        lat=location[1],
        lon=location[0],
        sen=sensor,
        key='AIzaSyBJoOn8_8flCKFCeJU0SgqD0kYj8EIWzEk'
    )

    url = "{base}{params}".format(base=base, params=params)

    try:
        data = json.loads(requests.get(url).text)
        if len(data['results']) > 0:
            result = data['results'][0]

        api_call_counter += 1

        street_name,street_number,postal_code,locality,city,country = "","","","","",""

        for component in result['address_components']:

            if 'route' in component['types']:
                street_name = component['long_name']
            if 'street_number' in component['types']:
                street_number = component['long_name']
            if 'postal_code' in component['types']:
                postal_code = component['long_name']
            if 'locality' in component['types']:
                locality = component['long_name']
            if 'administrative_area_level_2' in component['types']:
                city = component['long_name']
            if 'country' in component['types']:
                country = component['long_name']

        formatted_address = result['formatted_address']

        return {'formatted_address': formatted_address, 'street_name':street_name,'street_number':street_number,'postal_code':postal_code,'locality':locality,'city':city,'country':country}

    except requests.exceptions.ConnectionError as ex:

        print("Fout bij het maken van een connectie naar de google geocode API, gebruik --geocode=false.")

        exit(1)


def geocode_db_create_tables(dbcon):
    
    cursor = dbcon.cursor()

    cursor.execute('''
        CREATE TABLE places(key TEXT PRIMARY KEY,formatted_address TEXT,street_name TEXT,
        street_number TEXT,postal_code TEXT,locality TEXT,city TEXT,country TEXT)
    ''')

    dbcon.commit()


def geocodedb_get_handle():
    global geocodeDbHandle

    if not geocodeDbHandle:

        geocodeDbHandle = sqlite3.connect('geocode.sqlite')
        
        try:
            geocode_db_create_tables(geocodeDbHandle)
            
        except sqlite3.OperationalError:
            
            return geocodeDbHandle

        return geocodeDbHandle

    else:
        return geocodeDbHandle


def geocodedb_lookup_address(location):

    key = geolocmap.map_location_to_key(location)
    dbcon = geocodedb_get_handle()
    c = dbcon.cursor()

    c.execute('''SELECT * FROM places WHERE key = ?''', (key,))

    res = c.fetchone()

    if res:
        return {'formatted_address': res[1], 'street_name': res[2], 'street_number': res[3],
                'postal_code': res[4], 'locality': res[5], 'city': res[6], 'country': res[7]}
    else:
        return None

def geocodedb_cache_address(location, address):

    key = geolocmap.map_location_to_key(location)
    dbcon = geocodedb_get_handle()
    cursor = dbcon.cursor()

    res = cursor.execute('''
        INSERT INTO places(key,formatted_address,street_name,street_number,postal_code,locality,city,country) values (?,?,?,?,?,?,?,?)
        ''', (key, address['formatted_address'],address['street_name'],address['street_number'],address['postal_code'],address['locality'],address['city'],address['country']))

    dbcon.commit()

    
def get_address(location, call_google_api=False, use_caching=True):

    if use_caching:

        address = geocodedb_lookup_address(location)

        if not address and call_google_api:

            address = lookup_address_via_google_api(location)
            geocodedb_cache_address(location,address)

        return address


    elif call_google_api:

        return lookup_address_via_google_api(location)

    else:

        return None
