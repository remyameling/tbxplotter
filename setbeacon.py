import argparse,datetime
import include.validate as validate
import include.beacons as beacons



def main(beaconID,beaconName,reset, timestamp):

    print("{} {} {}".format(beaconID,beaconName,reset))

    beacon_data = beacons.get_beacon(beaconID)
    lastSplitted = None

    if beacon_data and not timestamp:
        lastSplitted = beacon_data[2]

    if reset and not timestamp:
        lastSplitted = None

    if timestamp:
        lastSplitted = timestamp.strftime("%d-%m-%y %H:%M:%S")

    beacons.insert_or_replace_beacon(beaconID, beaconName, lastSplitted)


if __name__ == '__main__':

    __author__ = "Remy Ameling"
    __description__ = "Bewaar baken naam en/of timestamp laatst gesplitst"
    __date__ = "20200424"

    parser = argparse.ArgumentParser(description=__description__ , epilog="Ontwikkeld door {} op {}".format(__author__ , __date__ ))

    parser.add_argument('bakenID', type=str, help='ID van het baken.')
    parser.add_argument('bakenNaam', type=str, help='naam van het baken.')
    parser.add_argument('-r','--reset', action='store_true',help="Reset timestamp van dit baken")
    parser.add_argument('-t','--timestamp', type=validate.dt, help="timestamp in UTC (dd-mm-yy hh:mm:ss)", default="")


    args = parser.parse_args()

    main(args.bakenID, args.bakenNaam, args.reset, args.timestamp)