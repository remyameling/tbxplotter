import argparse,csv,datetime
import include.reader as reader
import include.beacons as beacons
import include.validate as validate


def getFriendlyName(beaconID):
    beacon = beacons.get_beacon(beaconID)
    if beacon != None:
        return beacon[1]
    else:

        return "baken " + beaconID

def update_lastsplitted(beaconID,LastDateTime):

    beacons.insert_or_replace_beacon(beaconID,getFriendlyName(beaconID),LastDateTime)

def get_last_splitted(beacondID):

    beacon_data = beacons.get_beacon(beacondID)

    if beacon_data != '':
        if beacon_data[2] != None:
            return datetime.datetime.strptime(beacon_data[2], "%Y-%m-%d %H:%M:%S")

    return None;



def filter_by_dt(tbxdata,fromdt):

    data = []

    for record in tbxdata:

        date = record[3][0:2]+"-"+record[3][2:4]+"-"+record[3][4:6]
        time = record[4][0:2]+":"+record[4][2:4]+":"+record[4][4:6]
        dts  = date + ' ' + time
        dt   = datetime.datetime.strptime(dts, "%d-%m-%y %H:%M:%S")

        if dt > fromdt:
            data.append(record)

    return data


def get_filename(beacon_id):

    name = getFriendlyName(beacon_id)
    name = name.replace(" ", "-").lower()

    filename = beacon_id+"-"+name+".tbx"
    return filename


def main(filename, fromDate, newOnly, updateNew, listOnly):

    beacons = reader.read_beacons(filename)  # get all beacons for input file
    numFiles = 0

    for beacon_id in beacons:

        data = reader.filter_by_beacon(filename,beacon_id)

        if newOnly and not listOnly:
            fromDate = get_last_splitted(beacon_id)
            if fromDate == None:
                fromDate = ""

        if fromDate != "" and not listOnly:
            data = filter_by_dt(data,fromDate)

        if listOnly:
            fromDate = None

        numRecords = len(data)

        if numRecords > 0:
            firstDate = data[0][3][0:2]+"-"+data[0][3][2:4]+"-"+data[0][3][4:6]
            lastDate = data[-1][3][0:2]+"-"+data[-1][3][2:4]+"-"+data[-1][3][4:6]

            firstTime = data[0][4][0:2]+":"+data[0][4][2:4]+":"+data[0][4][4:6]
            lastTime = data[-1][4][0:2]+":"+data[-1][4][2:4]+":"+data[-1][4][4:6]

            firstTimeStamp = firstDate + ' ' + firstTime
            lastTimeStamp = lastDate + ' ' + lastTime

            if not listOnly:

                # create output .tbx file with records for the beacon only

                output_filename = get_filename(beacon_id)
                numFiles = numFiles + 1

                with open(output_filename,'w',newline='') as csvFile:
                    tbxWriter = csv.writer(csvFile, delimiter=',')

                    for record in data:
                        tbxWriter.writerow(record)

                # update last splitted time in beacons database

                if updateNew and not listOnly:
                    update_lastsplitted(beacon_id,lastTimeStamp)

                print("%27s : %6d records van %s tot %s gekopieerd naar %s" % (getFriendlyName(beacon_id), numRecords, firstTimeStamp, lastTimeStamp, output_filename))

            else:

                print("%27s : %6d records van %s tot %s" % (getFriendlyName(beacon_id), numRecords, firstTimeStamp, lastTimeStamp))

    if not listOnly and numFiles == 0:
        print("Geen records gevonden om te splitsen.")


if __name__ == '__main__':

    __author__ = "Remy Ameling"
    __description__ = "Splits tbx bestand met meerdere bakens in losse bestanden"
    __date__ = "20200424"

    parser = argparse.ArgumentParser(description=__description__ , epilog="Ontwikkeld door {} op {}".format(__author__ , __date__ ))

    parser.add_argument('filename', type=str, help='Naam van de .tbx file.')
    parser.add_argument('-l','--list', action='store_true',help="Alleen lijst van bakens tonen")
    parser.add_argument('-s', '--start', type=validate.dt, help='Filter op vanaf datum-tijd in UTC (dd-mm-yy hh:mm:ss)',default="")
    parser.add_argument('-n','--new', action='store_true', help="Alleen nieuwe records (sinds vorige run)")
    parser.add_argument('-u', '--update_new', action='store_true', help="Update laatste run datum en tijd (sinds vorige run)")

    args = parser.parse_args()

    main(args.filename, args.start, args.new, args.update_new, args.list)